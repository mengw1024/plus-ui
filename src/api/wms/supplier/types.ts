export interface SupplierVO {
  /**
   * 主键id
   */
  id: string | number;

  /**
   * 供应商编码
   */
  code: string;

  /**
   * 供应商名称
   */
  name: string;

  /**
   * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
   */
  type: string;

  /**
   * 网站网址
   */
  email: string;

  /**
   * 当前应付款
   */
  due: number;

  /**
   * 预付款
   */
  advance: number;

  /**
   * 省
   */
  province: string;

  /**
   * 市
   */
  city: string;

  /**
   * 区
   */
  district: string;

  /**
   * 详细地区
   */
  address: string;

  /**
   * 联系人
   */
  linkman: string;

  /**
   * 固话
   */
  landLine: string;

  /**
   * 手机
   */
  phone: string;

  /**
   * 1688供应商编码
   */
  alibabaSupplierCode: string;

  /**
   * 备注
   */
  remark: string;

}

export interface SupplierForm extends BaseEntity {
  /**
   * 主键id
   */
  id?: string | number;

  /**
   * 供应商编码
   */
  code?: string;

  /**
   * 供应商名称
   */
  name?: string;

  /**
   * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
   */
  type?: string;

  /**
   * 网站网址
   */
  email?: string;

  /**
   * 当前应付款
   */
  due?: number;

  /**
   * 预付款
   */
  advance?: number;

  /**
   * 省
   */
  province?: string;

  /**
   * 市
   */
  city?: string;

  /**
   * 区
   */
  district?: string;

  /**
   * 详细地区
   */
  address?: string;

  /**
   * 联系人
   */
  linkman?: string;

  /**
   * 固话
   */
  landLine?: string;

  /**
   * 手机
   */
  phone?: string;

  /**
   * 1688供应商编码
   */
  alibabaSupplierCode?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface SupplierQuery extends PageQuery {

  /**
   * 供应商编码
   */
  code?: string;

  /**
   * 供应商名称
   */
  name?: string;

  /**
   * 供应商类型 1.普通供应商 2.直供供应商 3.1688供应商
   */
  type?: string;

  /**
   * 网站网址
   */
  email?: string;

  /**
   * 当前应付款
   */
  due?: number;

  /**
   * 预付款
   */
  advance?: number;

  /**
   * 省
   */
  province?: string;

  /**
   * 市
   */
  city?: string;

  /**
   * 区
   */
  district?: string;

  /**
   * 详细地区
   */
  address?: string;

  /**
   * 联系人
   */
  linkman?: string;

  /**
   * 固话
   */
  landLine?: string;

  /**
   * 手机
   */
  phone?: string;

  /**
   * 1688供应商编码
   */
  alibabaSupplierCode?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



