export interface BrandsVO {
  /**
   * 品牌ID
   */
  id: string | number;

  /**
   * 品牌名
   */
  name: string;

  /**
   * 备注
   */
  remark: string;

}

export interface BrandsForm extends BaseEntity {
  /**
   * 品牌ID
   */
  id?: string | number;

  /**
   * 品牌名
   */
  name?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface BrandsQuery extends PageQuery {

  /**
   * 品牌名
   */
  name?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



