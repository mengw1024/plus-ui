import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { BrandsVO, BrandsForm, BrandsQuery } from '@/api/wms/brands/types';

/**
 * 查询品牌列表
 * @param query
 * @returns {*}
 */

export const listBrands = (query?: BrandsQuery): AxiosPromise<BrandsVO[]> => {
  return request({
    url: '/wms/brands/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询品牌详细
 * @param id
 */
export const getBrands = (id: string | number): AxiosPromise<BrandsVO> => {
  return request({
    url: '/wms/brands/' + id,
    method: 'get'
  });
};

/**
 * 新增品牌
 * @param data
 */
export const addBrands = (data: BrandsForm) => {
  return request({
    url: '/wms/brands',
    method: 'post',
    data: data
  });
};

/**
 * 修改品牌
 * @param data
 */
export const updateBrands = (data: BrandsForm) => {
  return request({
    url: '/wms/brands',
    method: 'put',
    data: data
  });
};

/**
 * 删除品牌
 * @param id
 */
export const delBrands = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/brands/' + id,
    method: 'delete'
  });
};
