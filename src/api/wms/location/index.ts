import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { LocationVO, LocationForm, LocationQuery } from '@/api/wms/location/types';

/**
 * 查询货位列表
 * @param query
 * @returns {*}
 */

export const listLocation = (query?: LocationQuery): AxiosPromise<LocationVO[]> => {
  return request({
    url: '/wms/location/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询货位详细
 * @param id
 */
export const getLocation = (id: string | number): AxiosPromise<LocationVO> => {
  return request({
    url: '/wms/location/' + id,
    method: 'get'
  });
};

/**
 * 新增货位
 * @param data
 */
export const addLocation = (data: LocationForm) => {
  return request({
    url: '/wms/location',
    method: 'post',
    data: data
  });
};

/**
 * 修改货位
 * @param data
 */
export const updateLocation = (data: LocationForm) => {
  return request({
    url: '/wms/location',
    method: 'put',
    data: data
  });
};

/**
 * 删除货位
 * @param id
 */
export const delLocation = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/location/' + id,
    method: 'delete'
  });
};
