export interface LocationVO {
  /**
   * 主键
   */
  id: string | number;

  /**
   * 仓库ID
   */
  warehouseId: string | number;

  /**
   * 仓库编码
   */
  warehouseCode: string;

  /**
   * 库区ID
   */
  areaId: string | number;

  /**
   * 库区编码
   */
  areaCode: string;

  /**
   * 货架ID
   */
  rackId: string | number;

  /**
   * 货架编码
   */
  rackCode: string;

  /**
   * 货位编码
   */
  code: string;

  /**
   * 货位类型
   */
  type: number;

  /**
   * ABC分类
   */
  abcType: string;

  /**
   * 重量上限
   */
  weight: number;

  /**
   * 体积上限
   */
  volume: number;

  /**
   * 补货触发量
   */
  reorderPoint: number;

  /**
   * 补货上限
   */
  restockCeiling: number;

  /**
   * 商品混放
   */
  commodityMixing: string;

  /**
   * 批次混放
   */
  batchMixing: string;

  /**
   * 排序
   */
  sort: number;

  /**
   * 尺寸-长
   */
  sizeLength: number;

  /**
   * 尺寸-宽
   */
  sizeWidth: string | number;

  /**
   * 尺寸-高
   */
  sizeHigh: number;

  /**
   * 排位层-排
   */
  levelRow: number;

  /**
   * 排位层-位
   */
  levelSlot: number;

  /**
   * 排位层-层
   */
  levelTier: number;

  /**
   * 备注
   */
  remark: string;

}

export interface LocationForm extends BaseEntity {
  /**
   * 主键
   */
  id?: string | number;

  /**
   * 仓库ID
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

  /**
   * 库区ID
   */
  areaId?: string | number;

  /**
   * 库区编码
   */
  areaCode?: string;

  /**
   * 货架ID
   */
  rackId?: string | number;

  /**
   * 货架编码
   */
  rackCode?: string;

  /**
   * 货位编码
   */
  code?: string;

  /**
   * 货位类型
   */
  type?: number;

  /**
   * ABC分类
   */
  abcType?: string;

  /**
   * 重量上限
   */
  weight?: number;

  /**
   * 体积上限
   */
  volume?: number;

  /**
   * 补货触发量
   */
  reorderPoint?: number;

  /**
   * 补货上限
   */
  restockCeiling?: number;

  /**
   * 商品混放
   */
  commodityMixing?: string;

  /**
   * 批次混放
   */
  batchMixing?: string;

  /**
   * 排序
   */
  sort?: number;

  /**
   * 尺寸-长
   */
  sizeLength?: number;

  /**
   * 尺寸-宽
   */
  sizeWidth?: string | number;

  /**
   * 尺寸-高
   */
  sizeHigh?: number;

  /**
   * 排位层-排
   */
  levelRow?: number;

  /**
   * 排位层-位
   */
  levelSlot?: number;

  /**
   * 排位层-层
   */
  levelTier?: number;

  /**
   * 备注
   */
  remark?: string;

}

export interface LocationQuery extends PageQuery {

  /**
   * 仓库ID
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

  /**
   * 库区ID
   */
  areaId?: string | number;

  /**
   * 库区编码
   */
  areaCode?: string;

  /**
   * 货架ID
   */
  rackId?: string | number;

  /**
   * 货架编码
   */
  rackCode?: string;

  /**
   * 货位编码
   */
  code?: string;

  /**
   * 货位类型
   */
  type?: number;

  /**
   * ABC分类
   */
  abcType?: string;

  /**
   * 重量上限
   */
  weight?: number;

  /**
   * 体积上限
   */
  volume?: number;

  /**
   * 补货触发量
   */
  reorderPoint?: number;

  /**
   * 补货上限
   */
  restockCeiling?: number;

  /**
   * 商品混放
   */
  commodityMixing?: string;

  /**
   * 批次混放
   */
  batchMixing?: string;

  /**
   * 排序
   */
  sort?: number;

  /**
   * 尺寸-长
   */
  sizeLength?: number;

  /**
   * 尺寸-宽
   */
  sizeWidth?: string | number;

  /**
   * 尺寸-高
   */
  sizeHigh?: number;

  /**
   * 排位层-排
   */
  levelRow?: number;

  /**
   * 排位层-位
   */
  levelSlot?: number;

  /**
   * 排位层-层
   */
  levelTier?: number;

    /**
     * 日期范围参数
     */
    params?: any;
}



