import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { SkuVO, SkuForm, SkuQuery } from '@/api/wms/sku/types';

/**
 * 查询商品规格列表
 * @param query
 * @returns {*}
 */

export const listSku = (query?: SkuQuery): AxiosPromise<SkuVO[]> => {
  return request({
    url: '/wms/sku/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询商品规格详细
 * @param id
 */
export const getSku = (id: string | number): AxiosPromise<SkuVO> => {
  return request({
    url: '/wms/sku/' + id,
    method: 'get'
  });
};

/**
 * 新增商品规格
 * @param data
 */
export const addSku = (data: SkuForm) => {
  return request({
    url: '/wms/sku',
    method: 'post',
    data: data
  });
};

/**
 * 修改商品规格
 * @param data
 */
export const updateSku = (data: SkuForm) => {
  return request({
    url: '/wms/sku',
    method: 'put',
    data: data
  });
};

/**
 * 删除商品规格
 * @param id
 */
export const delSku = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/sku/' + id,
    method: 'delete'
  });
};
