export interface SkuVO {
  /**
   * id
   */
  id: string | number;

  /**
   * 商品ID
   */
  goodId: string | number;

  /**
   * 规格编码
   */
  code: string;

  /**
   * 商品条码
   */
  barCode: string;

  /**
   * 颜色
   */
  color: string;

  /**
   * 重量
   */
  weight: number;

  /**
   * 体积
   */
  volume: number;

  /**
   * 长
   */
  length: number;

  /**
   * 宽
   */
  wide: string | number;

  /**
   * 高
   */
  high: number;

  /**
   * 售价
   */
  sellingPrice: number;

  /**
   * 批发价
   */
  tradePrice: number;

  /**
   * 进价
   */
  purchasePrice: number;

  /**
   * 备注
   */
  remark: string;

}

export interface SkuForm extends BaseEntity {
  /**
   * id
   */
  id?: string | number;

  /**
   * 商品ID
   */
  goodId?: string | number;

  /**
   * 规格编码
   */
  code?: string;

  /**
   * 商品条码
   */
  barCode?: string;

  /**
   * 颜色
   */
  color?: string;

  /**
   * 重量
   */
  weight?: number;

  /**
   * 体积
   */
  volume?: number;

  /**
   * 长
   */
  length?: number;

  /**
   * 宽
   */
  wide?: string | number;

  /**
   * 高
   */
  high?: number;

  /**
   * 售价
   */
  sellingPrice?: number;

  /**
   * 批发价
   */
  tradePrice?: number;

  /**
   * 进价
   */
  purchasePrice?: number;

  /**
   * 备注
   */
  remark?: string;

}

export interface SkuQuery extends PageQuery {

  /**
   * 商品ID
   */
  goodId?: string | number;

  /**
   * 规格编码
   */
  code?: string;

  /**
   * 商品条码
   */
  barCode?: string;

  /**
   * 颜色
   */
  color?: string;

  /**
   * 重量
   */
  weight?: number;

  /**
   * 体积
   */
  volume?: number;

  /**
   * 长
   */
  length?: number;

  /**
   * 宽
   */
  wide?: string | number;

  /**
   * 高
   */
  high?: number;

  /**
   * 售价
   */
  sellingPrice?: number;

  /**
   * 批发价
   */
  tradePrice?: number;

  /**
   * 进价
   */
  purchasePrice?: number;

    /**
     * 日期范围参数
     */
    params?: any;
}



