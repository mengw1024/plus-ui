export interface RackVO {
  /**
   * 主键
   */
  id: string | number;

  /**
   * 货架编码
   */
  code: string;

  /**
   * 货架类型
   */
  type: number;

  /**
   * 库区ID
   */
  areaId: string | number;

  /**
   * 库区编码
   */
  areaCode: string;

  /**
   * 仓库ID
   */
  warehouseId: string | number;

  /**
   * 仓库编码
   */
  warehouseCode: string;

  /**
   * 备注
   */
  remark: string;

}

export interface RackForm extends BaseEntity {
  /**
   * 主键
   */
  id?: string | number;

  /**
   * 货架编码
   */
  code?: string;

  /**
   * 货架类型
   */
  type?: number;

  /**
   * 库区ID
   */
  areaId?: string | number;

  /**
   * 库区编码
   */
  areaCode?: string;

  /**
   * 仓库ID
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface RackQuery extends PageQuery {

  /**
   * 货架编码
   */
  code?: string;

  /**
   * 货架类型
   */
  type?: number;

  /**
   * 库区ID
   */
  areaId?: string | number;

  /**
   * 库区编码
   */
  areaCode?: string;

  /**
   * 仓库ID
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



