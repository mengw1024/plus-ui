import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { RackVO, RackForm, RackQuery } from '@/api/wms/rack/types';

/**
 * 查询货架列表
 * @param query
 * @returns {*}
 */

export const listRack = (query?: RackQuery): AxiosPromise<RackVO[]> => {
  return request({
    url: '/wms/rack/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询货架详细
 * @param id
 */
export const getRack = (id: string | number): AxiosPromise<RackVO> => {
  return request({
    url: '/wms/rack/' + id,
    method: 'get'
  });
};

/**
 * 新增货架
 * @param data
 */
export const addRack = (data: RackForm) => {
  return request({
    url: '/wms/rack',
    method: 'post',
    data: data
  });
};

/**
 * 修改货架
 * @param data
 */
export const updateRack = (data: RackForm) => {
  return request({
    url: '/wms/rack',
    method: 'put',
    data: data
  });
};

/**
 * 删除货架
 * @param id
 */
export const delRack = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/rack/' + id,
    method: 'delete'
  });
};
