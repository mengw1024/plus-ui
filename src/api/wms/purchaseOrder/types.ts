export interface PurchaseOrderVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 业务时间
   */
  businessTime: string;

  /**
   * 单据编号
   */
  code: string;

  /**
   * 供应商ID
   */
  supplierManagementId: string | number;

  /**
   * 业务员
   */
  salesmanId: string | number;

  /**
   * 收获仓库ID
   */
  warehouseId: string | number;

  /**
   * 状态
   */
  status: string;

  /**
   * 商品总金额
   */
  goodsTotalMoney: number;

  /**
   * 采购运费
   */
  purchaseFreight: number;

  /**
   * 其他费用
   */
  otherCost: number;

  /**
   * 预付金额
   */
  amountAdvance: number;

  /**
   * 物流单号
   */
  trackNumber: string;

  /**
   * 总采购数量
   */
  totalPurchaseQuantity: number;

  /**
   * 已入库数量
   */
  storageQuantity: number;

  /**
   * 已入库金额
   */
  storageAmount: number;

  /**
   * 已发货数量
   */
  shippedQuantity: number;

  /**
   * 发货状态
   */
  shippedStatus: string;

  /**
   * 备注
   */
  remark: string;

}

export interface PurchaseOrderForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 业务时间
   */
  businessTime?: string;

  /**
   * 单据编号
   */
  code?: string;

  /**
   * 供应商ID
   */
  supplierManagementId?: string | number;

  /**
   * 业务员
   */
  salesmanId?: string | number;

  /**
   * 收获仓库ID
   */
  warehouseId?: string | number;

  /**
   * 状态
   */
  status?: string;

  /**
   * 商品总金额
   */
  goodsTotalMoney?: number;

  /**
   * 采购运费
   */
  purchaseFreight?: number;

  /**
   * 其他费用
   */
  otherCost?: number;

  /**
   * 预付金额
   */
  amountAdvance?: number;

  /**
   * 物流单号
   */
  trackNumber?: string;

  /**
   * 总采购数量
   */
  totalPurchaseQuantity?: number;

  /**
   * 已入库数量
   */
  storageQuantity?: number;

  /**
   * 已入库金额
   */
  storageAmount?: number;

  /**
   * 已发货数量
   */
  shippedQuantity?: number;

  /**
   * 发货状态
   */
  shippedStatus?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface PurchaseOrderQuery extends PageQuery {

  /**
   * 业务时间
   */
  businessTime?: string;

  /**
   * 单据编号
   */
  code?: string;

  /**
   * 供应商ID
   */
  supplierManagementId?: string | number;

  /**
   * 业务员
   */
  salesmanId?: string | number;

  /**
   * 收获仓库ID
   */
  warehouseId?: string | number;

  /**
   * 状态
   */
  status?: string;

  /**
   * 商品总金额
   */
  goodsTotalMoney?: number;

  /**
   * 采购运费
   */
  purchaseFreight?: number;

  /**
   * 其他费用
   */
  otherCost?: number;

  /**
   * 预付金额
   */
  amountAdvance?: number;

  /**
   * 物流单号
   */
  trackNumber?: string;

  /**
   * 总采购数量
   */
  totalPurchaseQuantity?: number;

  /**
   * 已入库数量
   */
  storageQuantity?: number;

  /**
   * 已入库金额
   */
  storageAmount?: number;

  /**
   * 已发货数量
   */
  shippedQuantity?: number;

  /**
   * 发货状态
   */
  shippedStatus?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



