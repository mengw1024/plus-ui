export interface WarehouseVO {
  /**
   * 主键
   */
  id: string | number;

  /**
   * 仓库编码
   */
  code: string;

  /**
   * 仓库名称
   */
  name: string;

  /**
   * 管理员ID
   */
  managerId: string | number;

  /**
   * 管理员姓名
   */
  managerName: string;

  /**
   * 联系地址
   */
  address: string;

  /**
   * 备注
   */
  remark: string;

}

export interface WarehouseForm extends BaseEntity {
  /**
   * 主键
   */
  id?: string | number;

  /**
   * 仓库编码
   */
  code?: string;

  /**
   * 仓库名称
   */
  name?: string;

  /**
   * 管理员ID
   */
  managerId?: string | number;

  /**
   * 管理员姓名
   */
  managerName?: string;

  /**
   * 联系地址
   */
  address?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface WarehouseQuery extends PageQuery {

  /**
   * 仓库编码
   */
  code?: string;

  /**
   * 仓库名称
   */
  name?: string;

  /**
   * 管理员ID
   */
  managerId?: string | number;

  /**
   * 管理员姓名
   */
  managerName?: string;

  /**
   * 联系地址
   */
  address?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



