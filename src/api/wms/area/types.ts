export interface AreaVO {
  /**
   * 主键
   */
  id: string | number;

  /**
   * 库区编码
   */
  code: string;

  /**
   * 库区名称
   */
  name: string;

  /**
   * 库区类型 1.捡货区 2.备货区
   */
  type: number;

  /**
   * 仓库id
   */
  warehouseId: string | number;

  /**
   * 仓库编码
   */
  warehouseCode: string;

  /**
   * 备注
   */
  remark: string;

}

export interface AreaForm extends BaseEntity {
  /**
   * 主键
   */
  id?: string | number;

  /**
   * 库区编码
   */
  code?: string;

  /**
   * 库区名称
   */
  name?: string;

  /**
   * 库区类型 1.捡货区 2.备货区
   */
  type?: number;

  /**
   * 仓库id
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface AreaQuery extends PageQuery {

  /**
   * 库区编码
   */
  code?: string;

  /**
   * 库区名称
   */
  name?: string;

  /**
   * 库区类型 1.捡货区 2.备货区
   */
  type?: number;

  /**
   * 仓库id
   */
  warehouseId?: string | number;

  /**
   * 仓库编码
   */
  warehouseCode?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



