import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { AreaVO, AreaForm, AreaQuery } from '@/api/wms/area/types';

/**
 * 查询库区列表
 * @param query
 * @returns {*}
 */

export const listArea = (query?: AreaQuery): AxiosPromise<AreaVO[]> => {
  return request({
    url: '/wms/area/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询库区详细
 * @param id
 */
export const getArea = (id: string | number): AxiosPromise<AreaVO> => {
  return request({
    url: '/wms/area/' + id,
    method: 'get'
  });
};

/**
 * 新增库区
 * @param data
 */
export const addArea = (data: AreaForm) => {
  return request({
    url: '/wms/area',
    method: 'post',
    data: data
  });
};

/**
 * 修改库区
 * @param data
 */
export const updateArea = (data: AreaForm) => {
  return request({
    url: '/wms/area',
    method: 'put',
    data: data
  });
};

/**
 * 删除库区
 * @param id
 */
export const delArea = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/area/' + id,
    method: 'delete'
  });
};
