export interface GoodsVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 商品编码
   */
  code: string;

  /**
   * 商品名称
   */
  name: string;

  /**
   * 品牌ID
   */
  brandId: string | number;

  /**
   * 单位
   */
  unit: string;

  /**
   * 商品标记
   */
  sign: string;

  /**
   * 货号
   */
  number: string;

  /**
   * 备注
   */
  remark: string;

}

export interface GoodsForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 商品编码
   */
  code?: string;

  /**
   * 商品名称
   */
  name?: string;

  /**
   * 品牌ID
   */
  brandId?: string | number;

  /**
   * 单位
   */
  unit?: string;

  /**
   * 商品标记
   */
  sign?: string;

  /**
   * 货号
   */
  number?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface GoodsQuery extends PageQuery {

  /**
   * 商品编码
   */
  code?: string;

  /**
   * 商品名称
   */
  name?: string;

  /**
   * 品牌ID
   */
  brandId?: string | number;

  /**
   * 单位
   */
  unit?: string;

  /**
   * 商品标记
   */
  sign?: string;

  /**
   * 货号
   */
  number?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



