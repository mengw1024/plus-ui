import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { GoodsVO, GoodsForm, GoodsQuery } from '@/api/wms/goods/types';

/**
 * 查询商品列表
 * @param query
 * @returns {*}
 */

export const listGoods = (query?: GoodsQuery): AxiosPromise<GoodsVO[]> => {
  return request({
    url: '/wms/goods/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询商品详细
 * @param id
 */
export const getGoods = (id: string | number): AxiosPromise<GoodsVO> => {
  return request({
    url: '/wms/goods/' + id,
    method: 'get'
  });
};

/**
 * 新增商品
 * @param data
 */
export const addGoods = (data: GoodsForm) => {
  return request({
    url: '/wms/goods',
    method: 'post',
    data: data
  });
};

/**
 * 修改商品
 * @param data
 */
export const updateGoods = (data: GoodsForm) => {
  return request({
    url: '/wms/goods',
    method: 'put',
    data: data
  });
};

/**
 * 删除商品
 * @param id
 */
export const delGoods = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/goods/' + id,
    method: 'delete'
  });
};
