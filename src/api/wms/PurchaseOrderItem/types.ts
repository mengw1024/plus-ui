export interface PurchaseOrderItemVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 业务时间
   */
  businessTime: string;

  /**
   * 供应商ID
   */
  supplierManagementId: string | number;

  /**
   * 业务员
   */
  salesmanId: string | number;

  /**
   * 收货仓库ID
   */
  warehouseId: string | number;

  /**
   * 规格编码
   */
  skuCode: string;

  /**
   * 颜色
   */
  color: string;

  /**
   * 重量
   */
  weight: number;

  /**
   * 体积
   */
  volume: number;

  /**
   * 长
   */
  length: number;

  /**
   * 宽
   */
  wide: string | number;

  /**
   * 高
   */
  high: number;

  /**
   * 批发价
   */
  tradePrice: number;

  /**
   * 进价
   */
  purchasePrice: number;

  /**
   * 备注
   */
  remark: string;

}

export interface PurchaseOrderItemForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 业务时间
   */
  businessTime?: string;

  /**
   * 供应商ID
   */
  supplierManagementId?: string | number;

  /**
   * 业务员
   */
  salesmanId?: string | number;

  /**
   * 收货仓库ID
   */
  warehouseId?: string | number;

  /**
   * 规格编码
   */
  skuCode?: string;

  /**
   * 颜色
   */
  color?: string;

  /**
   * 重量
   */
  weight?: number;

  /**
   * 体积
   */
  volume?: number;

  /**
   * 长
   */
  length?: number;

  /**
   * 宽
   */
  wide?: string | number;

  /**
   * 高
   */
  high?: number;

  /**
   * 批发价
   */
  tradePrice?: number;

  /**
   * 进价
   */
  purchasePrice?: number;

  /**
   * 备注
   */
  remark?: string;

}

export interface PurchaseOrderItemQuery extends PageQuery {

  /**
   * 业务时间
   */
  businessTime?: string;

  /**
   * 供应商ID
   */
  supplierManagementId?: string | number;

  /**
   * 业务员
   */
  salesmanId?: string | number;

  /**
   * 收货仓库ID
   */
  warehouseId?: string | number;

  /**
   * 规格编码
   */
  skuCode?: string;

  /**
   * 颜色
   */
  color?: string;

  /**
   * 重量
   */
  weight?: number;

  /**
   * 体积
   */
  volume?: number;

  /**
   * 长
   */
  length?: number;

  /**
   * 宽
   */
  wide?: string | number;

  /**
   * 高
   */
  high?: number;

  /**
   * 批发价
   */
  tradePrice?: number;

  /**
   * 进价
   */
  purchasePrice?: number;

    /**
     * 日期范围参数
     */
    params?: any;
}



