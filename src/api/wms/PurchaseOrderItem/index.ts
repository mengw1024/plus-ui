import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { PurchaseOrderItemVO, PurchaseOrderItemForm, PurchaseOrderItemQuery } from '@/api/wms/PurchaseOrderItem/types';

/**
 * 查询采购订单明细列表
 * @param query
 * @returns {*}
 */

export const listPurchaseOrderItem = (query?: PurchaseOrderItemQuery): AxiosPromise<PurchaseOrderItemVO[]> => {
  return request({
    url: '/wms/PurchaseOrderItem/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询采购订单明细详细
 * @param id
 */
export const getPurchaseOrderItem = (id: string | number): AxiosPromise<PurchaseOrderItemVO> => {
  return request({
    url: '/wms/PurchaseOrderItem/' + id,
    method: 'get'
  });
};

/**
 * 新增采购订单明细
 * @param data
 */
export const addPurchaseOrderItem = (data: PurchaseOrderItemForm) => {
  return request({
    url: '/wms/PurchaseOrderItem',
    method: 'post',
    data: data
  });
};

/**
 * 修改采购订单明细
 * @param data
 */
export const updatePurchaseOrderItem = (data: PurchaseOrderItemForm) => {
  return request({
    url: '/wms/PurchaseOrderItem',
    method: 'put',
    data: data
  });
};

/**
 * 删除采购订单明细
 * @param id
 */
export const delPurchaseOrderItem = (id: string | number | Array<string | number>) => {
  return request({
    url: '/wms/PurchaseOrderItem/' + id,
    method: 'delete'
  });
};
